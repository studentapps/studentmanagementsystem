package com.Browser.Utility;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class MyUtility {
	private static WebDriver driver; // null

	public void launchLocalBrowser(String browserName) {
		if(browserName.equalsIgnoreCase("Chrome")) {
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();                   // Run-time polymorphism
		} else if(browserName.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();
	}
}
	public void maximizeBrowser() {
		try {
		driver.manage().window().maximize();
		} catch (NullPointerException e) {
			System.out.println("Cannot maximize Browser failed to create Instance? call LaunchLocalBrowser");
		}
	}
	
	public void setResolution(Device device) {
		Dimension dimension;
		 if(device == Device.IPAD) {
		dimension=new Dimension(768, 1024);
		try {
			driver.manage().window().setSize(dimension);
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("Cannot change Screen size for the Browser failed to create Instance? call LaunchLocalBrowser");
		}
	} else if(device == Device.PIXEL) {
		dimension=new Dimension(360, 640);
		try {
			driver.manage().window().setSize(dimension);
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("Cannot change Screen size for the Browser failed to create Instance? call LaunchLocalBrowser");
		}
	} else if(device == Device.LAPTOP) {
		dimension=new Dimension(1280, 800);
		try {
			driver.manage().window().setSize(dimension);
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("Cannot change Screen size for the Browser failed to create Instance? call LaunchLocalBrowser");
		}
	}
		
	}
	
	public void setMobileResolution() {
		Dimension dimension=new Dimension(360, 640);
		try {
			driver.manage().window().setSize(dimension);
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("Cannot change Screen size for the Browser failed to create Instance? call LaunchLocalBrowser");
		}
	}
	
	public void closeActiveTab() {
		try {
			driver.close();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}
	
	public void closeBrowserSession() {
		try {
			driver.quit();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}
	
	public void goBack() {
		try {
			driver.navigate().back();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}
	
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}
	
	public void goForward() {
		try {
			driver.navigate().forward();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
	}
	
	public void goTo(String URL) {
		try {
			driver.get(URL);
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("Cannot load the page for the Browser failed to create Instance? call LaunchLocalBrowser");
			System.exit(0);
		}
	}
	
	public void enterText(By textBoxLocator,String texttoEnter) {
	try {
		WebElement element =driver.findElement(textBoxLocator);
		element.sendKeys(texttoEnter);
	}catch (NoSuchElementException e) {
		    System.err.println("Invalid Locator please check your Locator");
		    }
	catch (NullPointerException e) {
		System.out.println("Cannot load the page for the Browser failed to create Instance? call LaunchLocalBrowser");
		System.exit(0);
	}
	}
	
	public void clickOn(By locator) {
		try {
			WebElement element =driver.findElement(locator);
			element.click();
		}catch (NoSuchElementException e) {
			    System.err.println("Invalid Locator please check your Locator");
			    }
		catch (NullPointerException e) {
			System.out.println("Cannot load the page for the Browser failed to create Instance? call LaunchLocalBrowser");
			System.exit(0);
		}
	}
	
	public void selectFromdropdown(By dropDownLocator, int index) {
		try {
		WebElement dropDownElement=driver.findElement(dropDownLocator);
		Select roleDropDown=new Select(dropDownElement);
		roleDropDown.selectByIndex(index);
		} catch (NoSuchElementException e) {
		    System.err.println("Invalid Locator please check your Locator");
		    }
		catch (NullPointerException e) {
		System.out.println("Cannot load the page for the Browser failed to create Instance? call LaunchLocalBrowser");
		System.exit(0);
	}
}
	
	public void selectFromdropdown(By dropDownLocator, String visibleText) {
		try {
		WebElement dropDownElement=driver.findElement(dropDownLocator);
		Select roleDropDown=new Select(dropDownElement);
		roleDropDown.selectByVisibleText(visibleText);
		} catch (NoSuchElementException e) {
		    System.err.println("Invalid Locator please check your Locator");
		    }
		catch (NullPointerException e) {
		System.out.println("Cannot load the page for the Browser failed to create Instance? call LaunchLocalBrowser");
		System.exit(0);
	}
}
		
	}
