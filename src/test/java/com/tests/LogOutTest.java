package com.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Pages.AddUserPage;
import com.Pages.HomePage;

public class LogOutTest {
private HomePage homePage;
	
	@BeforeMethod
	public void setUp() throws InterruptedException {
		homePage=new HomePage();
		homePage.launchLocalBrowser("Chrome");
		homePage.goTo("http://68.183.9.82/");
		homePage.maximizeBrowser();
		homePage.goToLoginPage();
		Thread.sleep(2000);
	}
	
	@Test(testName="Log Out",description = "Test for LogOut")
	public void logOutTest() {
		AddUserPage addUserPage=new AddUserPage();
		addUserPage.logOut();
		
	}
	
	@AfterMethod
	public void delete() {
		try {
			homePage.closeBrowserSession();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
