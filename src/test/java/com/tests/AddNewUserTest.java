package com.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Pages.AddUserPage;
import com.Pages.HomePage;

public class AddNewUserTest {
private HomePage homePage;
	
	@BeforeMethod
	public void setUp() throws InterruptedException {
		homePage=new HomePage();
		homePage.launchLocalBrowser("Chrome");
		homePage.goTo("http://68.183.9.82/");
		homePage.maximizeBrowser();
		homePage.goToLoginPage();
		Thread.sleep(2000);
	}
	
	@Test(testName="Add New Student",description = "Test for Add a New User")
	public void addNewUserTest() throws InterruptedException {
		AddUserPage addUserPage=new AddUserPage();
		addUserPage.addNewUserRegister();
		
	}
	
	@AfterMethod
	public void delete() {
		try {
			AddUserPage addUserPage=new AddUserPage();
			addUserPage.deleteUser("John");
		} catch (Exception e) {
			// TODO: handle exception
		}
		homePage.closeBrowserSession();
	}
}
