package com.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Pages.AddUserPage;
import com.Pages.HomePage;

public class SearchFirstNameTest {
private HomePage homePage;
	
	@BeforeMethod
	public void setUp() throws InterruptedException {
		homePage=new HomePage();
		homePage.launchLocalBrowser("Chrome");
		homePage.goTo("http://68.183.9.82/");
		homePage.maximizeBrowser();
		homePage.goToLoginPage();
	}
	
	@Test(testName="Search FirstName",description = "Test for go to Search Box")
	public void SearchTest() throws InterruptedException {
		AddUserPage addUserPage=new AddUserPage();
		addUserPage.searchBox();
		
	}
	
	@AfterMethod
	public void delete() {
		try {
			homePage.closeBrowserSession();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
